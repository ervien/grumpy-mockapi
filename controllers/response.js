const success = {
  status: 'success',
  results: 'success',
  errors: ''
};

const failed = {
  status: 'failed',
  results: '',
  errors: 'failed'
};

module.exports = {
  success,
  failed,
};
