const jsonServer = require('json-server');

const mayapada = jsonServer.create();

// doctors endpoint
mayapada.use('/doctors', require('./doctors.js'));

// patients endpoint
mayapada.use('/patients', require('./patients.js'));

// appointments endpoint
mayapada.use('/appointments', require('./appointments.js'));

// schedules endpoint
mayapada.use('/schedules', require('./schedules'));

module.exports = mayapada;
