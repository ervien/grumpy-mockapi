const jsonServer = require('json-server');
const doctors = jsonServer.create();
const doctorList = require('../../data/mayapada/doctor_list.json');

doctors.get('/', function(req, res){
    res.status(200).send({
        'message': 'Success',
        'status': 200,
        'data': doctorList
    })
})
module.exports = doctors;
