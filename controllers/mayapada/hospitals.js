const jsonServer = require('json-server');
const hospitals = jsonServer.create();
const hospitalList = require('../../data/mayapada/hospital_list.json');

hospitals.get('/', function (req, res) {
  res.status(200).send({
    message: 'Success',
    status: 200,
    data: hospitalList,
  });
});

module.exports = hospitals;
