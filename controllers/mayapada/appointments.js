const jsonServer = require('json-server');
const appointments = jsonServer.create();
const low = require('lowdb');
const Memory = require('lowdb/adapters/Memory');
const response = require('../response');

const adapter = new Memory();
const data = require('../../data/mayapada');

const db = low(adapter).setState(data);

// create appointment
appointments.post('/', (req, res) => {
  const patient_id = Math.floor(Math.random() * 1000).toString();

  const payload = {
    ktp: req.body.ktp || '',
    name: req.body.name || '',
    gender: req.body.gender || '',
    phone_number: req.body.phone_number || '',
    dob: req.body.dob || '',
    patient_id,
  };

  const patient = db
    .get('appointments')
    .find({ ktp: payload.ktp })
    .value();

  if (patient) {
    response.failed.message = 'Duplicated patient data';
    return res.jsonp(response.failed);
  }

  db.get('appointments').push(payload).write();
  response.success.data = payload;
  return res.jsonp(response.success);
});

module.exports = appointments;
