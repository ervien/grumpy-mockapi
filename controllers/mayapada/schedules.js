const jsonServer = require('json-server');

const filter = require('lodash/filter');
const schedules = jsonServer.create();
const scheduleList = require('../../data/mayapada/schedule_list.json');

schedules.get('/', function (req, res) {
  const { from, to, hospital_id, doctor_code } = req.query;
  const filteredScheduleList = filter(scheduleList, (o) => {
    return (
      o.date >= from &&
      o.date <= to &&
      o.hospital_id === hospital_id &&
      o.doctor_code === doctor_code
    );
  });

  res.status(200).send({
    message: 'Success',
    status: 200,
    data: filteredScheduleList,
  });
});
module.exports = schedules;
