const jsonServer = require('json-server');
const patients = jsonServer.create();
const low = require('lowdb');
const Memory = require('lowdb/adapters/Memory');
const response = require('../response');

const adapter = new Memory();
const data = require('../../data/mayapada');

const db = low(adapter).setState(data);

patients.get('/', (req, res) => {
  const { name, dob, gender, phone_number } = req.query;

  const patient = db
    .get('patients')
    .find({ name, dob, gender, phone_number })
    .value();

  if (patient) {
    response.success.data = {
      ktp: patient.ktp,
      name: patient.name,
      gender: patient.gender,
      phone_number: patient.phone_number,
      patient_id: patient.patient_id,
    };

    return res.jsonp(response.success);
  }

  response.failed.message = 'Patient not found';

  return res.jsonp(response.failed);
});

patients.post('/', (req, res) => {
  const patient_id = Math.floor(Math.random() * 1000).toString();

  const payload = {
    ktp: req.body.ktp || '',
    name: req.body.name || '',
    gender: req.body.gender || '',
    phone_number: req.body.phone_number || '',
    dob: req.body.dob || '',
    patient_id,
  };

  const patient = db
    .get('patients')
    .find({ ktp: payload.ktp })
    .value();

  if (patient) {
    response.failed.message = 'Duplicated patient data';
    return res.jsonp(response.failed);
  }

  db.get('patients').push(payload).write();
  response.success.data = payload;
  return res.jsonp(response.success);
});

module.exports = patients;
