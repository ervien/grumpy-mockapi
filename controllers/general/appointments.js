const jsonServer = require('json-server');
const appointments = jsonServer.create();
const low = require('lowdb');
const Memory = require('lowdb/adapters/Memory');
const response = require('../response');

const adapter = new Memory();
const data = require('../../data/mayapada');

const db = low(adapter).setState(data);

appointments.post('/cancel-by-hospital', (req, res) => {
  const { appointment_id, reason } = req.body;

  const appointment = db
    .get('appointments')
    .find({ appointment_id })
    .value();

  if (appointment) {
    db.get('appointments')
      .find({ appointment_id })
      .assign({ canceled: true, reason })
      .write();

    response.success.status = 'success';
    response.success.results = 'Success cancel appointment';
    response.success.errors = [];
    return res.jsonp(response.success);
  }

  response.failed.status = 'failed';
  response.failed.results = [];
  response.failed.errors = ['Appointment not found.'];
  return res.status(404).jsonp(response.failed);
});

appointments.post ('/update-mrid', (req, res) => {
  const { appointment_id, mrid } = req.body;

  const appointment = db
    .get('appointments')
    .find({ appointment_id })
    .value();

  if (appointment) {
    db.get('patients')
      .find({ patient_id: appointment.patient_id })
      .assign({ mrid })
      .write();

    response.success.status = 'success';
    response.success.results = 'Success update mrid';
    response.success.errors = [];
    return res.jsonp(response.success);
  }

  response.failed.status = 'failed';
  response.failed.results = [];
  response.failed.errors = ['Data not found.'];
  return res.status(404).jsonp(response.failed);
})

module.exports = appointments;
