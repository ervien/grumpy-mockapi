const jsonServer = require('json-server');

const general = jsonServer.create();

// appointments endpoint
general.use('/appointments', require('./appointments'));

// schedules endpoint
general.use('/schedules', require('./schedules'));

module.exports = general;
