const jsonServer = require('json-server');
const schedules = jsonServer.create();
const low = require('lowdb');
const Memory = require('lowdb/adapters/Memory');
const response = require('../response');

const adapter = new Memory();
const data = require('../../data/mayapada');

const db = low(adapter).setState(data);

schedules.post('/sync', (req, res) => {
  const { date_from, date_to, list_of_schedules } = req.body;

  const scheduleList = data.schedules.filter(
    (x) => x.date >= date_from && x.date <= date_to,
  );

  list_of_schedules.forEach((schedule) => {
    const scheduleTarget = scheduleList.find(
      (o) => o.schedule_id === schedule.schedule_id,
    );

    if (scheduleTarget) {
      db.get('schedules')
        .find({ schedule_id: schedule.schedule_id })
        .assign({
          doctor_code: schedule.doctor_code,
          date: schedule.date,
          date_from: schedule.start_time,
          date_to: schedule.end_time,
          available_slot: schedule.available_slot
        })
        .write();
    }
  });

  response.success.status = 'success';
  response.success.results = 'Successfully sync schedules';
  response.success.errors = [];
  return res.jsonp(response.success);
});

module.exports = schedules;
