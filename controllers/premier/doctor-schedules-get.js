const filter = require('lodash/filter');
const data = require('../../data/premier');
const response = require('../response');

const getDoctorSchedules = (req, res) => {
  const { slotSchedules } = data;

  const filteredSlot = filter(slotSchedules, (o) => {
    return (
      o.doktercode === req.query.doktercode &&
      o.tanggal === req.query.date
    );
  });

  if (filteredSlot.length > 0) {
    response.success.data = filteredSlot;
    return res.jsonp(response.success);
  }

  return res.jsonp(response.failed);
};

module.exports = getDoctorSchedules;
