const filter = require('lodash/filter');
const find = require('lodash/find');
const isUndefined = require('lodash/isUndefined');
const data = require('../data');
const response = require('./response');

const getAppointment = (req, res) => {
  const appointmentList = filter(data.appointments, (o) => {
    return o.Tanggal >= req.query.from && o.Tanggal <= req.query.to;
  });

  if (appointmentList.length > 0) {
    const serializedAppointmentList = [];
    appointmentList.forEach((app) => {
      const patient = find(
        data.patients,
        (o) => o.ID === app.patientid,
      );

      const schedule = find(
        data.schedules,
        (sch) => sch.doktercode === app.doktercode,
      );

      if (isUndefined(patient)) return res.jsonp(response.failed);
      if (isUndefined(schedule)) return res.jsonp(response.failed);

      const obj = {
        ScheduleId: app.ScheduleId,
        ServiceProvider: 'Alodokter',
        FirstName: patient.FirstName,
        MiddleName: patient.MiddleName,
        LastName: patient.LastName,
        MedicalRecordNumber: null,
        PatientId: patient.ID,
        DateAppointment: schedule.tanggal,
        TimeAppointment: schedule.mulai,
        Specialist: schedule.Specialist,
        doctorcode: schedule.doktercode,
        Doctor: schedule.dokter,
        AdmissionStatus: 'Discharged',
      };

      serializedAppointmentList.push(obj);
    });

    response.success.data = serializedAppointmentList;
    return res.jsonp(response.success);
  }

  return res.jsonp(response.failed);
};

module.exports = getAppointment;
