const sortBy = require('lodash/sortBy');
const data = require('../../data/premier');
const response = require('../response');

const getCities = (req, res) => {
  const { cities } = data;

  if (cities) {
    const sortedCities = sortBy(cities, [(o) => o.CITYAREA_Desc]);
    response.success.data = sortedCities;
    return res.jsonp(response.success);
  }

  return res.jsonp(response.failed);
};

module.exports = getCities;
