const data = require('../../data/premier');
const response = require('../response');

const getGenders = (req, res) => {
  const sex = data.genders;

  if (sex) {
    response.success.data = sex;
    return res.jsonp(response.success);
  }

  return res.jsonp(response.failed);
};

module.exports = getGenders;
