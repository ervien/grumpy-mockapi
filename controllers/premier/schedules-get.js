const filter = require('lodash/filter');
const data = require('../../data/premier');
const response = require('../response');

const getSchedules = (req, res) => {
  const scheduleList = filter(data.schedules, (o) => {
    return o.tanggal >= req.query.from && o.tanggal <= req.query.to;
  });

  if (scheduleList.length > 0) {
    response.success.data = scheduleList;
    return res.jsonp(response.success);
  }

  return res.jsonp(response.failed);
};

module.exports = getSchedules;
