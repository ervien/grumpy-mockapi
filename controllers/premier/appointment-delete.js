const low = require('lowdb');
const Memory = require('lowdb/adapters/Memory');
const response = require('../response');

const adapter = new Memory();
const data = require('../../data/premier');

const db = low(adapter).setState(data);

const deleteAppointment = (req, res) => {
  const appointment = db
    .get('appointments')
    .find({ ScheduleId: req.query.scheduleid })
    .value();

  if (appointment) {
    db.get('appointments').remove({
      ScheduleId: req.query.scheduleid,
    });
    response.success.data = { ScheduleId: req.query.scheduleid };
    return res.jsonp(response.success);
  }

  return res.jsonp(response.failed);
};

module.exports = deleteAppointment;
