const omit = require('lodash/omit');
const low = require('lowdb');
const Memory = require('lowdb/adapters/Memory');
const faker = require('faker');
const response = require('../response');

const adapter = new Memory();
const data = require('../../data/premier');

const db = low(adapter).setState(data);

const postAppointment = (req, res) => {
  const { doktercode } = req.query;

  const schedule = db.get('schedules').find({ doktercode }).value();
  const randomID = `${Math.floor(
    Math.random() * 90,
  )}||${faker.random.number().toString()}`;

  db.get('appointments')
    .push({
      ScheduleId: randomID,
      No: Math.floor(Math.random() * 100),
      Tanggal: req.query.date,
      Jam: req.query.starttime,
      Dokter: schedule.dokter,
      patientid: req.query.patientid,
      doktercode: req.query.doktercode,
    })
    .write();

  const appointment = db
    .get('appointments')
    .find({ ScheduleId: randomID })
    .value();

  if (appointment) {
    response.success.data = omit(appointment, [
      'patientid',
      'doktercode',
    ]);
    return res.jsonp(response.success);
  }

  return res.jsonp(response.failed);
};

module.exports = postAppointment;
