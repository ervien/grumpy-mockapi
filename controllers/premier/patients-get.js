const filter = require('lodash/filter');
const data = require('../../data/premier');
const response = require('../response');

const splitedName = (nameQuery) =>
  nameQuery.split(' ').map((o) => o.toLowerCase());

const filterByName = (nameList, patient) => {
  return (
    nameList.includes(patient.FirstName.toLowerCase()) ||
    nameList.includes(patient.MiddleName.toLowerCase()) ||
    nameList.includes(patient.LastName.toLowerCase())
  );
};

const getPatients = (req, res) => {
  const patientList = filter(data.patients, (o) => {
    const nameList = splitedName(req.query.name);

    return (
      (filterByName(nameList, o) && o.Dob === req.query.dob) ||
      o.Sex === req.query.sex
    );
  });

  if (patientList.length > 0) {
    response.success.data = patientList;
    return res.jsonp(response.success);
  }

  return res.jsonp(response.failed);
};

module.exports = getPatients;
