const omit = require('lodash/omit');
const low = require('lowdb');
const moment = require('moment');
const Memory = require('lowdb/adapters/Memory');
const faker = require('faker');
const response = require('../response');

const adapter = new Memory();
const data = require('../../data/premier');

const db = low(adapter).setState(data);

const postPatients = (req, res) => {
  const obj = {};
  obj.ID = faker.random.number().toString();
  obj.PatientId = obj.ID;
  obj.FirstName = req.query.name1;
  obj.MiddleName = req.query.name2 || '';
  obj.LastName = req.query.name3 || '';
  obj.MrNo = '';
  obj.Address = req.query.address || '';
  obj.CityArea = req.query.cityarea || '';
  obj.MobilePhone = req.query.mobile || '';
  obj.Sex = req.query.sex || '';
  obj.email = req.query.email || '';
  obj.ktp = req.query.ktp || '';
  obj.Dob = moment(req.query.dob, 'DD-MM-YYYY').format('YYYY-MM-DD');
  db.get('patients')
    .push(omit(obj, ['PatientId']))
    .write();

  const serializedObj = omit(obj, ['ID']);

  response.success.data = serializedObj;

  return res.jsonp(response.success);
};

module.exports = postPatients;
