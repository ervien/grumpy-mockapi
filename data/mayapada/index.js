const appointments = require('./appointment_list.json');
const patients = require('./patient_list.json');
const schedules = require('./schedule_list.json');

module.exports = {
  appointments,
  patients,
  schedules,
};
