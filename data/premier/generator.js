const faker = require('faker/locale/id_ID');
const fs = require('fs');

const patients = [];
const genders = ['M', 'F'];
const religions = ['Islam', 'Protestan', 'Katolik'];

const scheduleList = require('./schedules');

// patients data
for (let index = 1; index <= 100; index += 1) {
  const patient = {};
  patient.ID = faker.random.number().toString();
  patient.FirstName = faker.name.firstName();
  patient.MiddleName = faker.name.lastName();
  patient.LastName = faker.name.lastName();
  patient.MrNo = faker.random.number().toString();
  patient.Dob = faker.date.past().toISOString().substring(0, 10);
  patient.Sex = genders[Math.floor(Math.random() * 2)];
  patient.Religion = religions[Math.floor(Math.random() * 3)];
  patient.Phone = faker.phone.phoneNumber();
  patient.MobilePhone = faker.phone.phoneNumber();
  patient.Email = `${patient.FirstName.toLowerCase()}${patient.LastName.toLowerCase()}@gmail.com`;
  patients.push(patient);
}

// fs.writeFile('data/patients.json', JSON.stringify(patients));

// schedules data
const schedules = [];
const specialties = ['Eye Center', 'Dokter Kandungan'];
const mulaiList = ['10:00:00', '11:00:00'];
const selesaiList = ['14:00:00', '17:00:00'];
for (let index = 1; index <= 100; index += 1) {
  schedules.push({
    doktercode: faker.random.number().toString().substring(0, 4),
    dokter: `${faker.name.firstName()} ${faker.name.lastName()}, dr.`,
    Spesialist: specialties[Math.floor(Math.random() * 2)],
    tanggal: faker.date.soon().toISOString().substring(0, 10),
    mulai: mulaiList[Math.floor(Math.random() * 2)],
    selesai: selesaiList[Math.floor(Math.random() * 2)],
  });
}

// fs.writeFile('data/schedules.json', JSON.stringify(schedules));

// city area
const cities = [];
for (let index = 1; index <= 100; index += 1) {
  cities.push({
    CITYAREA_Desc: faker.address.city(),
  });
}

// fs.writeFile('data/cities.json', JSON.stringify(cities));

// slot schedules
const slotSchedules = [];
const availabilities = ['available', 'not available'];
const times = [
  '10:00:00',
  '10:10:00',
  '10:20:00',
  '10:30:00',
  '10:40:00',
];
const randomID = `${Math.floor(
  Math.random() * 90,
)}||${faker.random.number().toString()}`;

scheduleList.forEach((schedule) => {
  times.forEach((time) => {
    slotSchedules.push({
      doktercode: schedule.doktercode,
      tanggal: schedule.tanggal,
      id: randomID,
      sesi: String(1),
      jam: time,
      numberofslot: Math.floor(Math.random() * 10),
      status: availabilities[Math.floor(Math.random() * 2)],
    });
  });
});

// fs.writeFile(
//   'data/slotSchedules.json',
//   JSON.stringify(slotSchedules),
// );
