const patients = require('./patients');
const schedules = require('./schedules');
const appointments = require('./appointments');
const genders = require('./genders');
const cities = require('./cities');
const slotSchedules = require('./slotSchedules');

const data = {
  patients,
  schedules,
  appointments,
  genders,
  cities,
  slotSchedules,
};

module.exports = data;
