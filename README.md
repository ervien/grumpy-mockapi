# Requirements
- Node v8.17+
- npm
- docker (optional)

# Getting Started
## Clone the repo:
```
  git clone git@bitbucket.org:anggiariksa/grumpy-mockapi.git
  cd grumpy-mockapi
```
## Run Without Docker
#### Install depedencies:
```
npm install
```

#### Running locally:
```
npm run start
```

## Run With Docker

#### Build docker images : 
```
docker build -t asia.gcr.io/alodokter-development/mock-api:latest .
```

#### Run container
```
docker run -p 3001:3001 -d asia.gcr.io/alodokter-development/mock-api:latest
```
#### Check container logs
   1. Get container id : `docker ps`
   2. Show app logs : `docker logs <containerid>` or `docker logs -f <containerid>`
#### If need to go inside the container
```
docker exec -it <containerid> bash
```

## Test Endpoint:
```
curl -X GET \
  -G "localhost:3001/api/pasien?name=agustina&dob=2020-10-26&sex=L"
  -H "Content-Type: application/json"

curl -X POST \
  -G localhost:3001/api/appointment/insert/alodokter?doktercode=8770&patientid=1234&date=2020-11-10&starttime=10:00:00&endtime=15:00:00"
  -H "Content-Type: application/json"
```

## References:
- [Setup Docker](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/)