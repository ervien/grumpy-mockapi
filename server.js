const jsonServer = require('json-server');
const mayapada = require('./controllers/mayapada');
const general = require('./controllers/general');

const server = jsonServer.create();
const middleware = jsonServer.defaults();

const PORT = 3001;
const HOST = '0.0.0.0';

server.use(middleware);
server.use(jsonServer.bodyParser);

server.use(
  '/api/v1',
  (req, res, next) => {
    next();
  },
  general,
);

server.use(
  '/api/v1/mayapada',
  (req, res, next) => {
    next();
  },
  mayapada,
);

server.listen(PORT, () => {
  console.log(`json server is running on http://${HOST}:${PORT}`);
});
